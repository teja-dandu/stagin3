const axios = require("axios");

const getCSRFToken = async () => {
  const res = await axios.get("https://stagingapp3.tricaequity.com/csrftoken", {
    withCredentials: true,
  });
  const { csrfToken } = res.data;
  return { CSRFToken: csrfToken, Cookie: res.headers["set-cookie"] };
};

getCSRFToken();
module.exports = {
  getCSRFToken,
};
