const request = require('supertest');
const utils = require("./utils.js");

const url = `https://stagingapp3.tricaequity.com`;


describe('login the stagingapp3 ', () => {
  it('responds with json with correct credentials', async function() {

    const data = {
      "email": "teja.dandu+02@trica.co",
      "password": "Teja@1364",
      "recaptchaToken": "HFeWlqahBLKRZraG9fV0NOVgEfHyodYB9oLxkDKBYwPSoieC4OHBk3JDdMWjNKPy4VbyxKHRoZUFxTTk4pD3IIf0wMGkZTZ315YTd_WU4pMihyQ1Y_VG9uQTNpYwZIV0UOBmhScEtZUHg7CB0xcCYtP1V3WllcDCh9fhd-P09FRBZre0JxakBzTAEfHyoaZw"
    }
    const { CSRFToken, Cookie } = await utils.getCSRFToken();
    await request(`${url}/api/v1.0/auth/login`)
      .post('/')
      .send(data)
      .set('Accept', 'application/json')
      .set("CSRF-Token", CSRFToken)
      .set("Cookie", Cookie)
      .expect(200)
      // .end(function(err, res) {
      //     console.log(res._body);
      //     if (err) throw err;
      //   });

  });

  it( 'Should be truthy', async () => {
    expect( true ).toBeTruthy();
  })
  it('responds with json password has given to be wrong', async function() {

    const data = {
      "email": "teja.dandu+02@trica.co",
      "password": "Teja@1",
      "recaptchaToken": "HFeWlqahBLKRZraG9fV0NOVgEfHyodYB9oLxkDKBYwPSoieC4OHBk3JDdMWjNKPy4VbyxKHRoZUFxTTk4pD3IIf0wMGkZTZ315YTd_WU4pMihyQ1Y_VG9uQTNpYwZIV0UOBmhScEtZUHg7CB0xcCYtP1V3WllcDCh9fhd-P09FRBZre0JxakBzTAEfHyoaZw"
    }

    await request(`${url}/api/v1.0/auth/login`)
      .post('/')
      .send(data)
      .set('Accept', 'application/json')
      .set('CSRF-Token', await utils.getCSRFToken())
      // .set('Cookie', Cookie)
      .expect(400)
      // .end(function(err, res) {
      //     console.log(res._body);
      //     if (err) throw err;
      //   });

  })

  it('responds with json email has given to be wrong', async function() {

    const data = {
      "email": ".dandu+02@trica.co",
      "password": "Teja@1364",
      "recaptchaToken": "HFeWlqahBLKRZraG9fV0NOVgEfHyodYB9oLxkDKBYwPSoieC4OHBk3JDdMWjNKPy4VbyxKHRoZUFxTTk4pD3IIf0wMGkZTZ315YTd_WU4pMihyQ1Y_VG9uQTNpYwZIV0UOBmhScEtZUHg7CB0xcCYtP1V3WllcDCh9fhd-P09FRBZre0JxakBzTAEfHyoaZw"
    }
    await request(`${url}/api/v1.0/auth/login`)
      .post('/')
      .send(data)
      .set('Accept', 'application/json')
      .set('CSRF-Token', await utils.getCSRFToken())
      // .set('Cookie', Cookie)
      .expect(400)
      // .end(function(err, res) {
      //     console.log(res._body);
      //     if (err) throw err;
      //   });

  })


});

