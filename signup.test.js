const utils = require("./utils.js");

const request = require("supertest");
const { v4: uuidv4 } = require("uuid");
uuidv4();

const url = `https://stagingapp3.tricaequity.com`;

describe("signup page of trica", () => {
  it("responds with json signup an new account", async function () {
    const data = {
      email: `teja.dandu+${uuidv4()}@trica.co`,
      phoneNumber: "+917349857693",
      firstName: "teja",
      lastName: "dandu",
      brandName: "Trica",
    };
    const { CSRFToken, Cookie } = await utils.getCSRFToken();
    await request(`${url}/api/v1.0/auth/signup`)
      .post("/")
      .send(data)
      .set("Accept", "application/json")
      .set("CSRF-Token", CSRFToken)
      .set("Cookie", Cookie)
      .expect(200);
    // .end((err, res) => {
    //     console.log(res._body);
    //     if (err) throw err;
    //   });
  });

  it("responds with json are already existed signup credentials", async function () {
    const data = {
      email: "teja.dandu+02@trica.co",
      phoneNumber: "+918989898989",
      firstName: "john",
      lastName: "dandu",
      brandName: "Trica",
    };
    await request(`${url}/api/v1.0/auth/signup`)
      .post("/")
      .send(data)
      .set("Accept", "application/json")
      .set("CSRF-Token", await utils.getCSRFToken())
      .expect(400);
    // .end(function(err, res) {
    //     if (err) throw err;
    //   });
  });
});
